# Git基础
@(Git)[Git学习]

---

 [toc]

## GitHub
[Git使用详细教程](http://www.admin10000.com/document/5374.html)
看到了分支管理策略，后面的还没看。很基础，全面，GitHub也能简单的操作操作
了。

## GitLab
[GitLab使用总结](https://blog.csdn.net/huaishu/article/details/50475175)
之前上来就是英文的说明文档，一小段一小段的翻译，只看了一半，懵懂中，看完这个我才知道怎么弄ssh
## Git教程
[Git教程](http://www.runoob.com/git/git-tutorial.html)
## Git可视化极简易教程 — Git GUI使用方法
[Git可视化极简易教程 — Git GUI使用方法](http://www.runoob.com/w3cnote/git-gui-window.html)
## Gitlab学习入门
[Gitlab学习入门](http://blog.csdn.net/u010775025/article/details/79863464)
## Git Documentation
[Book](https://git-scm.com/book/zh/v2)
这本书可以好好看看
## 其它介绍
[git&git使用指南](https://www.tuicool.com/articles/mEbAZbE)